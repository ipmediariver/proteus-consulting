@include('../includes.header')

<section class="pageTitle">
	<div class="container">
		<h1>@yield('pageTitle')</h1>
	</div>
</section>

<section class="interiorContent">
	<div class="container">
		<div class="row">
			<div class="col-sm-8">
				<div class="content">
					@yield('content')

					<a href="{{URL::to('/')}}" class="customBtn" style="margin-top: 2rem;">Back to home</a>
				</div>
			</div>
			<div class="col-sm-4 relative">
				<aside></aside>
				<div class="aside">
					<h2 class="newsTitle">News Rss</h2>
					<div id="divRss" class="newsRss"></div>
				</div>
			</div>
		</div>
	</div>
</section>

@include('../includes.footer')