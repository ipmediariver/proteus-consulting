<!doctype html>
<html class="no-js" lang="en">
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<title>Proteus App</title>

		{{ HTML::style('mobile/css/app.css') }}

	</head>
	<body>
	
	@yield('content')

	{{ HTML::script('https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js') }}
	{{ HTML::script('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js') }}
	{{ HTML::script('https://maps.googleapis.com/maps/api/js?key=&sensor=false&extension=.js') }}
	{{ HTML::script('mobile/js/render.components.js') }}

	</body>
</html>