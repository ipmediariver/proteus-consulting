<!DOCTYPE html>

<html lang="en">

  <head>

    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>PROTEUS CONSULTING | YOUR MEXICO RISK MANAGEMENT SPECIALIST</title>

    <link rel="stylesheet" href="{{asset('assets/css/app.css')}}">

    <link rel="icon" href="{{ asset('assets/img/favicon.png') }}">



    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->

    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

    <!--[if lt IE 9]>

      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>

      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

    <![endif]-->

  </head>

  <body>

    

    {{--HEADER MASTER--}}

    <header class="headerMaster">

      <div class="container">

        <div class="row">

          <div class="col-sm-4">

            <a href="{{URL::to('home')}}" class="logo">

              <img src="{{asset('assets/img/Logo-Proteus-Consulting.png')}}" alt="Logo Proteus Consulting">

            </a>

            <a href="#" class="mobileNavBtn">

              <i class="fa fa-bars"></i>

            </a>

          </div>

          <div class="col-sm-8">

            <h2 class="slogan">

              YOUR MEXICO RISK MANAGEMENT SPECIALIST
            </h2>

          </div>

        </div>

      </div>

    </header>

    

    {{--NAVIGATION--}}

    <nav class="navMaster">

      <div class="container">

        <div class="row">

          <div class="col-sm-6">

            <ul>

              <li><a href="{{URL::to('home')}}/">Home</a></li>

              <!-- <li><a href="#"><i class="fa fa-user"></i> Clients</a></li> -->

              <li><a href="{{URL::to('contact')}}/">Contact</a></li>

              <li><a href="{{URL::to('intranet')}}/">Intranet</a></li>

            </ul>

          </div>

          <div class="col-sm-6">

            <div class="phones">

              <!-- <h3><span>USA</span> (888) 640-8777 - <span>MX</span> 1-800-788-0888</h3> -->
              <h3>(619) 739-8085</h3>

            </div>

          </div>

        </div>

      </div>

    </nav>