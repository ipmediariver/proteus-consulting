<!-- <section class="clients">
      <div class="container">
        <div id="clientsProteus" class="carousel slide" data-ride="carousel" data-interval="2500">
          <div class="carousel-inner" role="listbox">
            <div class="item active">
              <div class="row">
                <div class="col-sm-2 col-xs-3 col-sm-offset-2">
                  <img src="{{asset('assets/img/brands-tactical-equipment/fieldline.jpg')}}" alt="Clients Proteus Consulting">
                </div>
                <div class="col-sm-2 col-xs-3">
                  <img src="{{asset('assets/img/brands-tactical-equipment/hatch.jpg')}}" alt="Clients Proteus Consulting">
                </div>
                <div class="col-sm-2 col-xs-3">
                  <img src="{{asset('assets/img/brands-tactical-equipment/camelbak.jpg')}}" alt="Clients Proteus Consulting">
                </div>
                <div class="col-sm-2 col-xs-3">
                  <img src="{{asset('assets/img/brands-tactical-equipment/blackhawk.jpg')}}" alt="Clients Proteus Consulting">
                </div>
              </div>
            </div>
            <div class="item">
              <div class="row">
                <div class="col-sm-2 col-xs-3 col-sm-offset-2">
                  <img src="{{asset('assets/img/brands-tactical-equipment/underarmour.jpg')}}" alt="Clients Proteus Consulting">
                </div>
                <div class="col-sm-2 col-xs-3">
                  <img src="{{asset('assets/img/brands-tactical-equipment/bates.jpg')}}" alt="Clients Proteus Consulting">
                </div>
                <div class="col-sm-2 col-xs-3">
                  <img src="{{asset('assets/img/brands-tactical-equipment/511.jpg')}}" alt="Clients Proteus Consulting">
                </div>
                <div class="col-sm-2 col-xs-3">
                  <img src="{{asset('assets/img/brands-tactical-equipment/voodoo.jpg')}}" alt="Clients Proteus Consulting">
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section> -->

    <footer class="footer--master">
      <div class="container">
        <div class="row">
          <div class="col-sm-4">
            <img src="{{asset('assets/img/Logo-Proteus-Texto.png')}}" class="logo" alt="Logo Proteus Consulting">
            <p style="color: #fff;"><small>Proteus Consulting® 2015</small></p>
          </div>
          <div class="col-sm-4 text-center">
            <img src="{{asset('assets/img/ifpo-logo.jpg')}}" style="height: 80px; margin-top: -1rem">
            <p style="color: #fff"><small>Corporate Member</small></p>
          </div>
          <div class="col-sm-4">
            <form action="" class="suscription">
              <label for="Email">Proteus Consulting | Suscription</label>
              <input type="email" name="Email" placeholder="Your email">
              <button type="submit"><i class="fa fa-check"></i></button>
            </form>
          </div>
        </div>
      </div>
    </footer>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
    <script src="{{asset('assets/js/FeedEk.js')}}"></script>
    <script src="{{asset('assets/js/app.js')}}"></script>
    <script src="{{asset('assets/js/mandrill.js')}}"></script>
  </body>
</html>