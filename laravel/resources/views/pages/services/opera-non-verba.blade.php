@extends('../layouts.interior')

@section('pageTitle')
	OPERA NON VERBA
@stop

@section('content')
	<img src="{{asset('assets/img/banners/banner11.jpg')}}" class="w-100 mb-4">

	<p>We serve fortune 500 companies with honor and distinction.</p>

	<p>Our core values and military provenance makes us proven to perform.</p>

	<p>We provide world class service for clients who require a strict standard of service.</p>
@stop