@extends('../layouts.interior')

@section('pageTitle')
	GRAVITAS INDEFINITA
@stop

@section('content')
	<img src="{{asset('assets/img/banners/banner15.jpg')}}" class="w-100 mb-4">

	<p>We are at home both in the US and Mexico.</p>

	<p>We are fully licensed to provide executive protection in Mexico.</p>
@stop