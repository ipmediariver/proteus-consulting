@extends('../layouts.interior')

@section('pageTitle')
	Executive Security Drivers
@stop

@section('content')
	<div class="clearfix">
		<img src="{{asset('assets/img/services/executive-protection.png')}}" class="thumb" alt="Executive Security Drivers - Proteus Consulting">
		<p>Proteus Consulting now offers our customary premium secure traveler transportation & logistics services at affordable prices for mid-level personnel traveling to Mexico.</p>
	</div>
	<ul>
		<li>Personal Security Detail</li>
		<li>Cross Border Security Driver</li>
		<li>Overall Executive Security Management</li>
	</ul>
@stop