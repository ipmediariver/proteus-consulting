@extends('../layouts.interior')

@section('pageTitle')
	EXECUTIVE PROTECTION AND SECURITY DETAILS
@stop

@section('content')
	<img src="{{asset('assets/img/banners/banner9.jpg')}}" class="w-100 mb-4">

	<p>We specialize in protecting groups and individuals from multinational companies in Mexico.</p>

	<p>Managers crossing the border daily to perform their duties.</p>

	<p>High level executives visiting corporate manufacturing facilities.</p>

	<p>Key suppliers and business partners temporarily performing key specific functions, whether they stay in Mexico or go back everyday.</p>

	<p>High visibility VIP’s for special events and functions.</p>
@stop