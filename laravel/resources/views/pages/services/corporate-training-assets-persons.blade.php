@extends('../layouts.interior')

@section('pageTitle')
	Geo-location of Assets & Persons
@stop

@section('content')
	<div class="clearfix">
		<img src="{{asset('assets/img/services/geolocation.jpg')}}" class="thumb" alt="Corporate Training of Assets & Persons - Proteus Consulting">
		<p>We keep discrete secure tracking of you while on travel. We are ready to respond in case of an emergency.  We locate you and extract you wherever you are.  In case of natural disaster we will come and rescue you.</p>
	</div>
	<ul>
 		<li>Secure Travel Logistics</li>
		<li>Travel Following & Crisis Response</li>
	</ul>
@stop