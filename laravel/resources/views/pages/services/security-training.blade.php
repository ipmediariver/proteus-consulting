@extends('../layouts.interior')

@section('pageTitle')
	SECURITY TRAINING
@stop

@section('content')
	<img src="{{asset('assets/img/banners/banner6.jpg')}}" class="w-100 mb-4">

	<p>Counter kidnap and crime avoidance training, defensive and evasive driving course</p>
	
	<div class="row sm">
		<div class="col-sm-6">
			<ul>
				<li>Tactical Armed & Un-armed Training</li>
				<li>Personal Security Details Certification</li>
				<li>EMTR (Emergency Medical Tactical Response)</li>
				<li>Crime Prevention and Awareness Courses</li>
			</ul>
		</div>
		<div class="col-sm-6">
			<ul>
				<li>Whole Family Contingency Courses</li>
				<li>Tactical Evasive/Defensive Driver Training</li>
			</ul>
		</div>
	</div>
@stop