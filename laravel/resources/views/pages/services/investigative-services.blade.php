@extends('../layouts.interior')



@section('pageTitle')

	Investigative Services

@stop



@section('content')

	<div class="clearfix">

		<img src="{{asset('assets/img/services/investigation2.jpg')}}" class="thumb" alt="Investigative Services - Proteus Consulting">

		<p>Do you know where your employees have been? We have an advantage when presenting Investigation and Security options for clients- being able to see their entire Security Profile.</p>

	</div>

	<div class="row">

		<div class="col-sm-6">

			<ul>

				<li>MEXICO Background Check</li>

				<li>Corporate Investigations</li>

				<li>Employee Trustworthiness</li>

			</ul>

		</div>

		<div class="col-sm-6">

			<ul>

				<li>Loss Prevention/Mitigation</li>

				<li>Risk/Threat Assessment</li>

			</ul>

		</div>

	</div>

@stop