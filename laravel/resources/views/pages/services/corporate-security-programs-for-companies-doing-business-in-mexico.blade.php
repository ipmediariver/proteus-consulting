@extends('../layouts.interior')

@section('pageTitle')
	CORPORATE SECURITY PROGRAMS FOR COMPANIES DOING BUSINESS IN MEXICO
@stop

@section('content')
	<img src="{{asset('assets/img/banners/banner5.jpg')}}" class="w-100 mb-4">

	<p>We tailor security programs for our customers, based on their specific threat and risk atmospherics.</p>

	<p>Our strategies effectively mitigate your risk and allow you to focus on what is important for your business.</p>
@stop