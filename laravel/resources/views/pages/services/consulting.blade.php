@extends('../layouts.interior')

@section('pageTitle')
	Consulting
@stop

@section('content')
	<div class="clearfix">
		<img src="{{asset('assets/img/services/consulting.png')}}" class="thumb" alt="Consulting - Proteus Consulting">
		<p>The most effective security programs are those that are based on preventive strategies and infrastructures, rather than waiting for the problem to occur and then taking corrective measures.</p>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<ul>
				<li>Crime Intelligence Mapping (MEXICO)</li>
				<li>Risk/Threat Assessment</li>
				<li>Complete Security Programs</li>
				<li>CSO Outsourcing</li>
				<li>Augmented Due Diligence</li>
			</ul>
		</div>
		<div class="col-sm-6">
			<ul>
				<li>Loss Prevention Programs</li>
				<li>Augmented CCTV & Access Control</li>
				<li>Disaster Recovery/ EVAC Programs</li>
				<li>Medical Emergency & Contingency</li>
			</ul>
		</div>
	</div>
@stop