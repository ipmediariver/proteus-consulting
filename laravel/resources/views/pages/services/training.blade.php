@extends('../layouts.interior')

@section('pageTitle')
	Training
@stop

@section('content')
	<div class="clearfix">
		<img src="{{asset('assets/img/services/training.jpg')}}" class="thumb" alt="Training - Proteus Consulting">
		<p>Counter kidnap and crime avoidance training, defensive and evasive driving course</p>
	</div>
	<div class="row sm">
		<div class="col-sm-6">
			<ul>
				<li>Tactical Armed & Un-armed Training</li>
				<li>Personal Security Details Certification</li>
				<li>EMTR (Emergency Medical Tactical Response)</li>
				<li>Crime Prevention and Awareness Courses</li>
			</ul>
		</div>
		<div class="col-sm-6">
			<ul>
				<li>Whole Family Contingency Courses</li>
				<li>Tactical Evasive/Defensive Driver Training</li>
			</ul>
		</div>
	</div>
@stop