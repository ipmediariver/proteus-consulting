@extends('../layouts.interior')

@section('pageTitle')
	UAV PLATFORMS FOR LAW ENFORCEMENT / SEARCH AND RESCUE
@stop

@section('content')
	<img src="{{asset('assets/img/banners/banner8.jpg')}}" class="w-100 mb-4">

	<p>We have customised platforms for every mission.</p>

	<p>Technical advice and special consulting available.</p>
@stop