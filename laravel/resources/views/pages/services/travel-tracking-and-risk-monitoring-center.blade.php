@extends('../layouts.interior')

@section('pageTitle')
	TRAVEL TRACKING AND RISK MONITORING CENTER
@stop

@section('content')
	<img src="{{asset('assets/img/banners/banner10.jpg')}}" class="w-100 mb-4">

	<p>We provide geolocation tracking and advice to our customers traveling throughout Mexico.</p>

	<p>Our TTRMC is the nerve center for our crisis response program and our risk prevention center.</p>

	<p>Our customers are global companies that require prompt reliable information about individuals and companies alike; be it a possible high level employee or a future business partners. We help mitigate the risk of dealing with persons or entities that of unknown provenance.</p>
@stop