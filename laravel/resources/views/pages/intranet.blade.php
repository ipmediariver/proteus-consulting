@extends('../layouts.interior')

@section('pageTitle')
	Intranet
@stop

@section('content')
	<p>Click on image to download it</p>
	<p><small>©All pictures are copyrighted by their respective authors</small></p>
	<hr>
	<div class="row">
		<div class="col-sm-4">
			<a href="{{asset('assets/img/intranet/proteus-consulting-wallpaper-1.jpg')}}">
				<div class="thumb_intranet" style="background-image: url('{{asset('assets/img/intranet/proteus-consulting-wallpaper-1.jpg')}}')"></div>
			</a>
		</div>
		<div class="col-sm-4">
			<a href="{{asset('assets/img/intranet/proteus-consulting-wallpaper-2.jpg')}}">
				<div class="thumb_intranet" style="background-image: url('{{asset('assets/img/intranet/proteus-consulting-wallpaper-2.jpg')}}')"></div>
			</a>
		</div>
		<div class="col-sm-4">
			<a href="{{asset('assets/img/intranet/proteus-consulting-wallpaper-3.jpg')}}">
				<div class="thumb_intranet" style="background-image: url('{{asset('assets/img/intranet/proteus-consulting-wallpaper-3.jpg')}}')"></div>
			</a>
		</div>
		<div class="col-sm-4">
			<a href="{{asset('assets/img/intranet/proteus-consulting-wallpaper-4.jpg')}}">
				<div class="thumb_intranet" style="background-image: url('{{asset('assets/img/intranet/proteus-consulting-wallpaper-4.jpg')}}')"></div>
			</a>
		</div>
		<div class="col-sm-4">
			<a href="{{asset('assets/img/intranet/proteus-consulting-wallpaper-5.jpg')}}">
				<div class="thumb_intranet" style="background-image: url('{{asset('assets/img/intranet/proteus-consulting-wallpaper-5.jpg')}}')"></div>
			</a>
		</div>
		<div class="col-sm-4">
			<a href="{{asset('assets/img/intranet/proteus-consulting-wallpaper-6.jpg')}}">
				<div class="thumb_intranet" style="background-image: url('{{asset('assets/img/intranet/proteus-consulting-wallpaper-6.jpg')}}')"></div>
			</a>
		</div>
		<div class="col-sm-4">
			<a href="{{asset('assets/img/intranet/proteus-consulting-wallpaper-7.jpg')}}">
				<div class="thumb_intranet" style="background-image: url('{{asset('assets/img/intranet/proteus-consulting-wallpaper-7.jpg')}}')"></div>
			</a>
		</div>
		<div class="col-sm-4">
			<a href="{{asset('assets/img/intranet/proteus-consulting-wallpaper-8.jpg')}}">
				<div class="thumb_intranet" style="background-image: url('{{asset('assets/img/intranet/proteus-consulting-wallpaper-8.jpg')}}')"></div>
			</a>
		</div>
		<div class="col-sm-4">
			<a href="{{asset('assets/img/intranet/proteus-consulting-wallpaper-9.jpg')}}">
				<div class="thumb_intranet" style="background-image: url('{{asset('assets/img/intranet/proteus-consulting-wallpaper-9.jpg')}}')"></div>
			</a>
		</div>
		<div class="col-sm-4">
			<a href="{{asset('assets/img/intranet/proteus-consulting-wallpaper-10.jpg')}}">
				<div class="thumb_intranet" style="background-image: url('{{asset('assets/img/intranet/proteus-consulting-wallpaper-10.jpg')}}')"></div>
			</a>
		</div>
		<div class="col-sm-4">
			<a href="{{asset('assets/img/intranet/proteus-consulting-wallpaper-11.jpg')}}">
				<div class="thumb_intranet" style="background-image: url('{{asset('assets/img/intranet/proteus-consulting-wallpaper-11.jpg')}}')"></div>
			</a>
		</div>
		<div class="col-sm-4">
			<a href="{{asset('assets/img/intranet/proteus-consulting-wallpaper-12.jpg')}}">
				<div class="thumb_intranet" style="background-image: url('{{asset('assets/img/intranet/proteus-consulting-wallpaper-12.jpg')}}')"></div>
			</a>
		</div>
		<div class="col-sm-4">
			<a href="{{asset('assets/img/intranet/proteus-consulting-wallpaper-13.jpg')}}">
				<div class="thumb_intranet" style="background-image: url('{{asset('assets/img/intranet/proteus-consulting-wallpaper-13.jpg')}}')"></div>
			</a>
		</div>
	</div>
@stop