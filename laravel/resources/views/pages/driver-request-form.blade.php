@extends('../layouts.interior')



@section('pageTitle')

	Executive Protection / Driver Request Form

@stop



@section('content')

	<form action="{{ url('/request-form') }}" method="post" id="request--form">
	
		{{ csrf_field() }}

		<div class="alert alert-danger" id="request--form__danger" role="alert" style="display: none">

	      <strong>Warning!</strong> <span id="danger--msg"></span>

	    </div>



	    <div class="alert alert-success" style="display: none" id="request--form__success" role="alert">

		  <strong>Thank you!</strong> We will contact you as soon as posible. Proteus Consulting™

		</div>	



		<div class="row">

			<div class="col-sm-6">

				<label>Fullname</label>

				<input type="text" name="request-fullname">

			</div>

			<div class="col-sm-6">

				<label>Email</label>

				<input type="email" name="request-email">

			</div>

		</div>

		<div class="row">

			<div class="col-sm-6">

				<label>Phone</label>

				<input type="text" name="request-phone">

			</div>

			<div class="col-sm-6">

				<label>Company</label>

				<input type="text" name="request-company">

			</div>

		</div>

		<label>Area(s) of Operation where service is needed.</label>

		<textarea name="request-question1"></textarea>

		<label>How many Principles (Clients) are we responsible for?</label>

		<textarea name="request-question2"></textarea>

		<label>Do you need an Armored Vehicle? <small style="color: red; font-size: 1.1rem">Note: In some locations this is not an option.</small></label>

		<textarea name="request-question3"></textarea>

		<label>What are the dates for this service?</label>

		<textarea name="request-question4"></textarea>

		<label>Do Principles  have  any <span style="color: red;">MEDICAL</span> conditions  that  we  should  be  aware of?</label>

		<textarea name="request-question5"></textarea>

		<footer>

			<button type="submit" class="customBtn" style="border: 0 !important;">Send message <i class="fa fa-angle-right"></i></a>

			<span class="sending--msg">Sending form...</span>

		</footer>

	</form>	

@stop