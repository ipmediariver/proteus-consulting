<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Proteus Consulting | Binational Risk Management Specialist</title>
    <link rel="stylesheet" href="{{asset('assets/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/app.css')}}">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    
    <div class="container">
      <div class="row">
        <div class="col-sm-6 col-sm-offset-3">
          <img src="{{asset('assets/img/Logo-Proteus-Consulting.png')}}" alt="Logo Proteus Consulting" class="componentsLogo">
          <h1>Lorem ipsum ad his</h1>
          <h2>Consectetur adipisicing elit</h2>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corrupti itaque debitis ex nulla in, fugiat velit quis, blanditiis tenetur ipsum libero. Ducimus maiores, ipsum asperiores eveniet saepe et architecto repellat?</p>
          <small>Lorem ipsum dolor sit amet, consectetur adipisicing elit</small>
          <hr>
          <div>
            <h2>Boton mini</h2>
            <a href="#" class="customBtn mini">Read more <i class="fa fa-caret-right"></i></a>
            <a href="#" class="customBtn primary mini">Read more <i class="fa fa-caret-right"></i></a>
          </div>

          <div>
            <h2>Boton normal</h2>
            <a href="#" class="customBtn">Read more <i class="fa fa-caret-right"></i></a>
            <a href="#" class="customBtn primary">Read more <i class="fa fa-caret-right"></i></a>
          </div>

          <div>
            <h2>Boton grande</h2>
            <a href="#" class="customBtn large">Read more <i class="fa fa-caret-right"></i></a>
            <a href="#" class="customBtn primary large">Read more <i class="fa fa-caret-right"></i></a>
          </div>
          
          <hr>
          
          <h2>Quote</h2>
          <blockquote>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloribus hic enim placeat id repellat, repudiandae tempore aut autem. Necessitatibus, nam, earum. Veritatis porro pariatur illum iusto tenetur necessitatibus nobis repellat.</p>
            <footer>
              <p>Lorem ipsum dolor sit amet</p>
            </footer>
          </blockquote>

          <h2>Formularios</h2>
          <form action="">
            <header>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
            </header>
            
            <div class="row">
              <div class="col-sm-6">
                <label for="Name">Name</label>
                <input type="text" placeholder="Full name">
              </div>
              <div class="col-sm-6">
                <label for="Email">Email</label>
                <input type="text" placeholder="Your email">
              </div>
            </div>

            <label for="Message">Message</label>
            <textarea name="" placeholder="Your message"></textarea>

            <footer>
              <a href="#" class="customBtn mini">Send message<i class="fa fa-caret-right"></i></a>
            </footer>
          </form>
        </div>
      </div>
    </div>

    <section class="darkSection">
      <div class="container">
        <div class="row">
          <div class="col-sm-6 col-sm-offset-3">
            <h1>Lorem ipsum ad his</h1>
            <h2>Consectetur adipisicing elit</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corrupti itaque debitis ex nulla in, fugiat velit quis, blanditiis tenetur ipsum libero. Ducimus maiores, ipsum asperiores eveniet saepe et architecto repellat?</p>
            <small>Lorem ipsum dolor sit amet, consectetur adipisicing elit</small>
            <hr>
            <div>
              <h2>Boton mini</h2>
              <a href="#" class="customBtn mini">Read more <i class="fa fa-caret-right"></i></a>
              <a href="#" class="customBtn primary mini">Read more <i class="fa fa-caret-right"></i></a>
            </div>

            <div>
              <h2>Boton normal</h2>
              <a href="#" class="customBtn">Read more <i class="fa fa-caret-right"></i></a>
              <a href="#" class="customBtn primary">Read more <i class="fa fa-caret-right"></i></a>
            </div>

            <div>
              <h2>Boton grande</h2>
              <a href="#" class="customBtn large">Read more <i class="fa fa-caret-right"></i></a>
              <a href="#" class="customBtn primary large">Read more <i class="fa fa-caret-right"></i></a>
            </div>
            
            <hr>
            
            <h2>Quote</h2>
            <blockquote>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloribus hic enim placeat id repellat, repudiandae tempore aut autem. Necessitatibus, nam, earum. Veritatis porro pariatur illum iusto tenetur necessitatibus nobis repellat.</p>
              <footer>
                <p>Lorem ipsum dolor sit amet</p>
              </footer>
            </blockquote>

            <h2>Formularios</h2>
            <form action="">
              <header>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
              </header>
              
              <div class="row">
                <div class="col-sm-6">
                  <label for="Name">Name</label>
                  <input type="text" placeholder="Full name">
                </div>
                <div class="col-sm-6">
                  <label for="Email">Email</label>
                  <input type="text" placeholder="Your email">
                </div>
              </div>

              <label for="Message">Message</label>
              <textarea name="" placeholder="Your message"></textarea>

              <footer>
                <a href="#" class="customBtn mini">Send message<i class="fa fa-caret-right"></i></a>
              </footer>
            </form>
          </div>
        </div>
      </div>
    </section>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
    <script src="{{asset('assets/js/app.js')}}"></script>
  </body>
</html>