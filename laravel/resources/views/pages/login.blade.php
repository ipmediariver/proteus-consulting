
    {{ Form::open(array('url' => 'app/login')) }}
    <h1>Login</h1>

    <!-- if there are login errors, show them here -->
    <p>
      {{ $errors->first('user') }}
      {{ $errors->first('password') }}
    </p>

    <p>
      {{ Form::label('user', 'User') }}
      {{ Form::text('user', Input::old('user'), array('placeholder' => 'User')) }}
    </p>

    <p>
      {{ Form::label('password', 'Password') }}
      {{ Form::password('password') }}
    </p>

    <p>{{ Form::submit('Submit!') }}</p>
  {{ Form::close() }}

