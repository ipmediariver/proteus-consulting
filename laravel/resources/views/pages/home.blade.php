@extends('../layouts.master')
@section('content')
{{--SLIDER--}}
<section class="slider">
    <div class="container">
        <div id="proteusHomeSlider" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner" role="listbox">
                <div class="item active">
                    <a href="service/executive-secure-transportation">
                        <img src="{{asset('assets/img/banners/banner7.jpg')}}" alt="EXECUTIVE DRIVERS">
                        <div class="description">
                            <h1>EXECUTIVE SECURE TRANSPORTATION</h1>
                            <p>We provide turnkey secure transportation solutions for your single VIP or group of VIP’s.</p>
                        </div>
                    </a>
                </div>



                
                <div class="item">
                    <a href="#">
                        <img src="{{asset('assets/img/banners/inertnal_security_services.png')}}" alt="INTERNAL SECURITY SERVICES">
                        <div class="description">
                            <h1>INTERNAL SECURITY SERVICES</h1>
                            <p>Ensure your establishment, facilities, storage and events security</p>
                        </div>
                    </a>
                </div>



                <!-- <div class="item">
                    <a href="service/executive-protection-and-security-details">
                        <img src="{{asset('assets/img/banners/banner9.jpg')}}" alt="EXECUTIVE PROTECTION">
                        <div class="description">
                            <h1>EXECUTIVE PROTECTION AND SECURITY DETAILS</h1>
                            <p>We specialize in protecting groups and individuals from multinational companies in Mexico.</p>
                        </div>
                    </a>
                </div> -->




                <div class="item">
                    <a href="service/corporate-security-programs-for-companies-doing-business-in-mexico">
                        <img src="{{asset('assets/img/banners/banner5.jpg')}}" alt="THREAT ANALYSIS AND RISK MITIGATION">
                        <div class="description">
                            <h1>CORPORATE SECURITY PROGRAMS FOR COMPANIES DOING BUSINESS IN MEXICO</h1>
                            <p>We tailor security programs for our customers, based on their specific threat and risk atmospherics.</p>
                        </div>
                    </a>
                </div>


                <div class="item">
                    <a href="service/security-training">
                        <img src="{{asset('assets/img/banners/banner6.jpg')}}" alt="SECURITY TRAINING">
                        <div class="description">
                            <h1>SECURITY TRAINING</h1>
                            <p>A Full Array of Training Solutions for Your Security Force.</p>
                        </div>
                    </a>
                </div>


                <div class="item">
                    <a href="#">
                        <img src="{{asset('assets/img/banners/veritas.png')}}" alt="VERITAS - TRUST VERIFICATION CENTERS">
                        <div class="description">
                            <h1>VERITAS - TRUST VERIFICATION CENTERS</h1>
                            <p>A Full Array of Training Solutions for Your Security Force.</p>
                        </div>
                    </a>
                </div>




                <!-- <div class="item">
                    <a href="service/gravitas-indefinita">
                        <img src="{{asset('assets/img/banners/banner15.jpg')}}" alt="SECURE EXECUTIVE TRANSPORT">
                        <div class="description">
                            <h1>GRAVITAS INDEFINITA</h1>
                            <p>We are at home both in the US and Mexico.</p>
                        </div>
                    </a>
                </div>
                <div class="item">
                    <a>
                        <img src="{{asset('assets/img/banners/banner13.jpg')}}" alt="SECURE EXECUTIVE TRANSPORT">
                        <div class="description">
                            <h1>AUGMENTED DUE DILIGENCE AND BACKGROUND CHECKS</h1>
                            <p>From deep background checks, to poligraph testing, we give you the answers you need.</p>
                        </div>
                    </a>
                </div>
                <div class="item">
                    <a href="service/opera-non-verba">
                        <img src="{{asset('assets/img/banners/banner11.jpg')}}" alt="SECURE EXECUTIVE TRANSPORT">
                        <div class="description">
                            <h1>OPERA NON VERBA</h1>
                            <p>We serve fortune 500 companies with honor and distinction</p>
                        </div>
                    </a>
                </div> -->



                <div class="item">
                    <a href="service/travel-tracking-and-risk-monitoring-center">
                        <img src="{{asset('assets/img/banners/banner14.jpg')}}" alt="SECURE EXECUTIVE TRANSPORT">
                        <div class="description">
                            <h1>TRAVEL TRACKING AND RISK MONITORING CENTER</h1>
                            <p>We provide geolocation tracking and advice to our customers traveling throughout Mexico.</p>
                        </div>
                    </a>
                </div>
                <div class="item">
                    <a href="service/uav-platforms-for-law-enforcement-search-and-rescue">
                        <img src="{{asset('assets/img/banners/banner8.jpg')}}" alt="LAW ENFORCEMENT / SEARCH AND RESCUE">
                        <div class="description">
                            <h1>UAV PLATFORMS FOR LAW ENFORCEMENT / SEARCH AND RESCUE</h1>
                            <p>We have customized platforms for every mission.</p>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <a href="#proteusHomeSlider" role="button" data-slide="prev" class="carousel--control prev">
        <i class="fa fa-angle-left"></i>
    </a>
    <a href="#proteusHomeSlider" role="button" data-slide="next" class="carousel--control next">
        <i class="fa fa-angle-right"></i>
    </a>
</section>

{{--DESCARGAR FORMULARIO--}}
<section class="downloadRequest">
    <div class="container">
        <div class="row">
            <div class="col-sm-8">
                <h2>Executive Protection / Driver Request Form <i class="fa fa-arrow-right"></i></h2>
            </div>
            <div class="col-sm-4 text-right">
                <a href="{{URL::to('/executive-protection-and-driver-request-form')}}" class="requestPdfBtn">
                    <i class="fa fa-file"></i> Fill our Request Form
                </a>
            </div>
        </div>
    </div>
</section>
{{--SERVICIOS PROTEUS CONSULTING--}}
<section class="servicesHome">
    <div class="container">
        
        <div class="row">
            <div class="col-sm-4">
                <a href="service/executive-secure-transportation">
                    <div class="serviceCard">
                        <div class="thumb" data-thumb="4">
                            <header>
                                <h2>Executive Secure Transportation
                                </h2>
                            </header>
                            <img src="{{asset('assets/img/services/tactical.jpg')}}" alt="Executive Security Transportation
                            - Proteus Consulting">
                        </div>
                        <div class="description">
                            <ul>
                                <li>One Time Services</li>
                                <li>Extended Time Contracts</li>
                                <li>Executive Protection Drivers Available</li>
                            </ul>
                        </div>
                        <footer>
                            <a href="service/executive-secure-transportation" class="customBtn primary mini">Read more <i class="fa fa-caret-right"></i></a>
                        </footer>
                    </div>
                </a>
            </div>
            <div class="col-sm-4">
                <a href="service/executive-protection-and-security-details">
                    <div class="serviceCard">
                        <div class="thumb" data-thumb="2">
                            <header>
                                <h2>Executive Protection and Security Details</h2>
                            </header>
                        </div>
                        <div class="description">
                            <ul>
                                <li>Personal Security Details</li>
                                <li>Cross Border Teams Available</li>
                                <li>Armored Vehicle Rentals</li>
                            </ul>
                        </div>
                        <footer>
                            <a href="service/executive-protection-and-security-details" class="customBtn primary mini">Read more <i class="fa fa-caret-right"></i></a>
                        </footer>
                    </div>
                </a>
            </div>
            <div class="col-sm-4">
                <a href="consulting">
                    <div class="serviceCard">
                        <div class="thumb" data-thumb="3">
                            <header>
                                <h2>Consulting</h2>
                            </header>
                        </div>
                        <div class="description">
                            <ul>
                                <li>Overall Executive Security Management</li>
                                <li>Risk/Threat Assessment</li>
                                <li>Complete Security Programs</li>
                            </ul>
                        </div>
                        <footer>
                            <a href="consulting" class="customBtn primary mini">Read more <i class="fa fa-caret-right"></i></a>
                        </footer>
                    </div>
                </a>
            </div>
            <div class="col-sm-4">
                <a href="investigative-services">
                    <div class="serviceCard">
                        <div class="thumb" data-thumb="1">
                            <header>
                                <h2>Investigative Services and Background Checks</h2>
                            </header>
                        </div>
                        <div class="description">
                            <ul>
                                <li>MEXICO Background Check</li>
                                <li>Corporate Investigations</li>
                                <li>Employee Trustworthiness</li>
                            </ul>
                        </div>
                        <footer>
                            <a href="investigative-services" class="customBtn primary mini">Read more <i class="fa fa-caret-right"></i></a>
                        </footer>
                    </div>
                </a>
            </div>
            <div class="col-sm-4">
                <a href="training">
                    <div class="serviceCard">
                        <div class="thumb" data-thumb="5">
                            <header>
                                <h2>Training</h2>
                            </header>
                            <img src="{{asset('assets/img/services/training.jpg')}}" alt="Training - Proteus Consulting">
                        </div>
                        <div class="description">
                            <ul>
                                <li>Defense Training for Executives</li>
                                <li>Personal Security Detail Certification</li>
                                <li>EMTR <small>(Emergency Medical Tactical Response)</small></li>
                            </ul>
                        </div>
                        <footer>
                            <a href="training" class="customBtn primary mini">Read more <i class="fa fa-caret-right"></i></a>
                        </footer>
                    </div>
                </a>
            </div>
            <div class="col-sm-4">
                <a href="corporate-training-assets-persons">
                    <div class="serviceCard">
                        <div class="thumb" data-thumb="6">
                            <header>
                                <h2>Geo-location of Assets & Persons</h2>
                            </header>
                            <img src="{{asset('assets/img/services/geolocation.jpg')}}" alt="crisis response - Proteus Consulting">
                        </div>
                        <div class="description">
                            <ul>
                                <li>Secured Travel Logistics</li>
                                <li>Travel Following & Crisis Response</li>
                                <li>Emergency Rescue and Medical Evacuation</li>
                            </ul>
                        </div>
                        <footer>
                            <a href="corporate-training-assets-persons" class="customBtn primary mini">Read more <i class="fa fa-caret-right"></i></a>
                        </footer>
                    </div>
                </a>
            </div>
            
        </div>
    </div>
</section>
@stop