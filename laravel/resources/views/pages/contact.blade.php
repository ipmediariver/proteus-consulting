@extends('../layouts.interior')

@section('pageTitle')
	Contact us
@stop

@section('content')
	<form action="">
		<header>
			<p>Fill in the fields below to contact us</p>
		</header>
		<label for="Name">Full name</label>
		<input type="text" name="Name" placeholder="Full name">
		<div class="row">
			<div class="col-sm-6">
				<label for="Company">Company</label>
				<input type="text" name="Company" placeholder="Company">
			</div>
			<div class="col-sm-6">
				<label for="Phone">Phone</label>
				<input type="text" name="Phone" placeholder="Phone">
			</div>
		</div>
		<label for="Email">Email</label>
		<input type="text" name="Email" placeholder="Email">
		<label for="Subject">Subject</label>
		<input type="text" name="Subject" placeholder="Subject">
		<label for="Message">Message</label>
		<textarea name="Message" placeholder="Your message..."></textarea>
		<footer>
			<a href="#" class="customBtn">Send message</a>
		</footer>
	</form>	
@stop