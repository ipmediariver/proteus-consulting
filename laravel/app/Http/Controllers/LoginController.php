<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

class LoginController extends Controller {

	public function showLogin()
	{
		return view('app.login');
	}


	public function index()
	{

		return view('app.login');
	}


	public function doLogin()
	{
		$rules = array(
			'user'    	=> 'required',
			'password' 	=> 'required|alphaNum|min:3'
		);

		$validator = Validator::make(Input::all(), $rules);

		// print "<pre>";
		// print $validator->fails();
		// die();

		if ($validator->fails()) {
			return Redirect::to('app')
				->withErrors($validator) 
				->withInput(Input::except('password'));
		} else {

			$userdata = array(
				'user' 	=> Input::get('user'),
				'password' 	=> Input::get('password')
			);


			print_r($userdata);


			if (Auth::attempt($userdata, false)) {

				return Redirect::intended('app');

			} else {	 	

				return Redirect::to('app/login');

			}

		}
	}
	public function doLogout()
	{
		Auth::logout(); 
		return Redirect::to('app/login');
	}

}