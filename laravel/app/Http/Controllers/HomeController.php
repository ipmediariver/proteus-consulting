<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

class HomeController extends Controller {

	public function index()
	{
		return view('pages.home');
	}

	public function intranet()
	{
		return view('pages.intranet');
	}

}