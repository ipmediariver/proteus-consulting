<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

class ConsultingController extends Controller {

	public function index()
	{
		return view('pages.services.consulting');
	}

}
