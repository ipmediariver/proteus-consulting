<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

class ServicesController extends Controller {

	public function index($service)
	{
		return view('pages.services.'.$service);
	}

}
