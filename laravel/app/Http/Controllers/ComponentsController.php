<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

class ComponentsController extends Controller {

	public function index()
	{
		return view('pages.components');
	}

}
