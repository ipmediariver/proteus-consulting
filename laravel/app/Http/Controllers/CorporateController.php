<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

class CorporateController extends Controller {

	public function index()
	{
		return view('pages.services.corporate-training-assets-persons');
	}

}
