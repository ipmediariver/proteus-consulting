<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', function () {
    return view('welcome');
});

Route::get('/components', 'ComponentsController@index');
Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@index');
Route::get('/intranet', 'HomeController@intranet');
Route::get('/contact', 'ContactController@index');
Route::get('/executive-protection-and-driver-request-form', 'RequestformController@index');
Route::get('/executive-security-transportation', 'TacticalController@index');
Route::get('/executive-security-drivers', 'ExecutiveController@index');
Route::get('/consulting', 'ConsultingController@index');
Route::get('/investigative-services', 'InvestigativeController@index');
Route::get('/training', 'TrainingController@index');
Route::get('/corporate-training-assets-persons', 'CorporateController@index');
Route::get('/service/{service?}', 'ServicesController@index');


Route::post('request-form', 'RequestformController@request_form');



// RUTAS PARA APP
Route::get('app/login', 'LoginController@index');
Route::post('app/login', 'LoginController@doLogin');
Route::get('app/logout', 'LoginController@doLogout');

Route::get('app', 'AppController@index');
// Route::get('app', array('before' => 'auth', 'uses' => 'AppController@index'));

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    //
});
