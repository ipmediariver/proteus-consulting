var app = {

    render: function(){
        app.components.brokenLink();
        app.components.mobileBtn();
        app.components.feedEk.news();
        app.components.mandrill.requestForm.click();
    },

    win: {
        load: function(){
            $(window).load(function(){

            });
        },

        scroll: function(){
            $(window).scroll(function(){
                var scrollTop = $(this).scrollTop();
            });
        }
    },

    components: {
        brokenLink: function(){
            $('a[href="#"]').click(function(i){
                i.preventDefault();
            });        
        },

        mobileBtn: function(){
            $('.mobileNavBtn').click(function(){
                $('nav ul').slideToggle('mobileNav');
            });
        },

        feedEk: {
            news: function(){
                $('#divRss').FeedEk({
                    FeedUrl:'http://www.huffingtonpost.com/tag/mexico-drug-wars/feed',
                    MaxCount : 3,
                    ShowDesc : true,
                    ShowPubDate:true,
                    DescCharacterLimit:100,
                    TitleLinkTarget:'_blank',
                });
            }
        },

        mandrill: {
            requestForm: {
                click: function(){
                    $('#send--form').click(function(){
                        var value = {
                            fullname:  $('[name="request-fullname"]').val(),
                            email:     $('[name="request-email"]').val(),
                            phone:     $('[name="request-phone"]').val(),
                            company:   $('[name="request-company"]').val(),
                            question1: $('[name="request-question1"]').val(),
                            question2: $('[name="request-question2"]').val(),
                            question3: $('[name="request-question3"]').val(),
                            question4: $('[name="request-question4"]').val(),
                            question5: $('[name="request-question5"]').val(),
                        }

                        app.components.mandrill.requestForm.validate(value);
                    });
                },

                validate: function(v){
                    var emailReg = new RegExp(/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);
                    var emailvalid = emailReg.test(v.email);
                    $('#request--form__success').hide();
                    if(v.fullname == '' || v.phone == '' || v.company == ''){
                        $('#danger--msg').text('Please verify your personal information.').closest('.alert').show();
                        $('html, body').animate({scrollTop: 0});
                    }else if(v.question1 == '' || v.question2 == '' || v.question3 == '' || v.question4 == '' || v.question5 == ''){
                        $('#danger--msg').text('Please answer all questions.').closest('.alert').show();
                        $('html, body').animate({scrollTop: 0});
                    }else if(!emailvalid){
                        $('#danger--msg').text('Please enter a valid email.').closest('.alert').show();
                        $('html, body').animate({scrollTop: 0});
                    }else{
                        $('.sending--msg').addClass('active');
                        app.components.mandrill.requestForm.send(v);
                    }
                },

                send: function(v){
                    var m = new mandrill.Mandrill('AEh-zBT8FP_4FOzbHxpqBQ');
                    var params = {
                        "template_name": "proteus-request-form",
                        "template_content": [],
                        "message": {
                            "subject": "Proteus Consulting - Request Form",
                            "from_email": v.email,
                            "from_name": v.name,
                            "to": [{
                                "email": "info@proteusconsulting.com",
                                "name": "Proteus Consulting",
                                "type": "to"
                            }],
                            "headers": {
                                "Reply-To": "info@proteusconsulting.com"
                            },
                            "merge": true,
                            "merge_language": "mailchimp",
                            "global_merge_vars": [{
                                "name": "merge1",
                                "content": "merge1 content"
                            }],
                            "merge_vars": [{
                                "rcpt": "info@proteusconsulting.com",
                                "vars": [{
                                    "name": "FULLNAME",
                                    "content": v.fullname
                                }, {
                                    "name": "EMAIL",
                                    "content": v.email
                                }, {
                                    "name": "PHONE",
                                    "content": v.phone
                                }, {
                                    "name": "COMPANY",
                                    "content": v.company
                                }, {
                                    "name": "QUESTION1",
                                    "content": v.question1
                                }, {
                                    "name": "QUESTION2",
                                    "content": v.question2
                                }, {
                                    "name": "QUESTION3",
                                    "content": v.question3
                                }, {
                                    "name": "QUESTION4",
                                    "content": v.question4
                                }, {
                                    "name": "QUESTION5",
                                    "content": v.question5
                                }]
                            }],
                        }
                    }
                    var async = false;
                    var ip_pool = "";
                    var send_at = "YYYY-MM-DD HH:MM:SS";
                    m.messages.sendTemplate(params, function(result) {
                        $('#request--form__danger').hide();
                        $('#request--form__success').show();
                        $('.sending--msg').removeClass('active');
                        $('[name*="request-"]').val('');
                        $('html, body').animate({scrollTop: 0});
                    }, function(e) {
                        alert('Please verify your information!');
                    });
                }
            }
        }
    },


}

app.render();
app.win.load();
app.win.scroll();
